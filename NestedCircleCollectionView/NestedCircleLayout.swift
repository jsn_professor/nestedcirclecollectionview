//
//  CircleLayout.swift
//  iOSTemplate
//
//  Created by Professor on 2018/11/15.
//  Copyright © 2018 Agrowood. All rights reserved.
//

import UIKit

public class NestedCircleLayout: UICollectionViewLayout {
    private var types = [NestedCircleType]()
    private var parent = [(parentIndex: Int, childCount: Int)]()
    var parentCenter = CGPoint.zero
    var parentRadius: CGFloat = 0
    var childRadius: CGFloat = 0
    public weak var delegate: NestedCircleLayoutDelegate?
    override public var collectionViewContentSize: CGSize {
        return collectionView?.bounds.size ?? .zero
    }

    override public func prepare() {
        if let collectionView = collectionView, let delegate = delegate {
            let minLength = min(collectionView.bounds.width, collectionView.bounds.height)
            parentCenter = CGPoint(x: collectionView.bounds.midX, y: collectionView.bounds.midY)
            parentRadius = minLength / 4
            childRadius = parentRadius * 0.6
            types.removeAll()
            parent.removeAll()
            for index in 0..<collectionView.numberOfItems(inSection: 0) {
                let type = delegate.nestedCicleLayout(itemTypeAt: index)
                types.append(type)
                if type == .parent {
                    parent.append((parent.count, 0))
                } else if parent.count > 0 {
                    parent[parent.count - 1].childCount += 1
                }
            }
        }
    }

    override public func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        let index = parentIndex(index: indexPath.item)
        if index < 0 {
            return nil
        }
        let parentAngle = 2 * .pi * CGFloat(index) / CGFloat(parent.count) - .pi / 2
        switch types[indexPath.item] {
        case .parent:
            attributes.center = CGPoint(x: parentCenter.x + parentRadius * cos(parentAngle), y: parentCenter.y + parentRadius * sin(parentAngle))
        case .child:
            let angle = 2 * .pi * CGFloat(childIndex(index: indexPath.item)) / CGFloat(parent[index].childCount) + parentAngle
            let center = CGPoint(x: parentCenter.x + parentRadius * cos(parentAngle), y: parentCenter.y + parentRadius * sin(parentAngle))
            attributes.center = CGPoint(x: center.x + childRadius * cos(angle), y: center.y + childRadius * sin(angle))
        }
        attributes.size = delegate?.nestedCicleLayout(sizeForItemAt: indexPath.item) ?? .zero
        return attributes
    }

    func childCenters() -> [CGPoint] {
        var points = [CGPoint]()
        for index in 0..<types.count {
            if types[index] == .child {
                let index = parentIndex(index: index)
                if index < 0 {
                    continue
                }
                let parentAngle = 2 * .pi * CGFloat(index) / CGFloat(parent.count) - .pi / 2
                points.append(CGPoint(x: parentCenter.x + parentRadius * cos(parentAngle), y: parentCenter.y + parentRadius * sin(parentAngle)))
            }
        }
        return points
    }

    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return (0..<types.count).compactMap { item -> UICollectionViewLayoutAttributes? in
            return self.layoutAttributesForItem(at: IndexPath(item: item, section: 0))
        }
    }

    private func parentIndex(index: Int) -> Int {
        var count = -1
        for index in 0...index {
            if types[index] == .parent {
                count += 1
            }
        }
        return count
    }

    private func childIndex(index: Int) -> Int {
        var count = 0
        for index in (0..<index).reversed() {
            if types[index] == .child {
                count += 1
            } else {
                break
            }
        }
        return count
    }

    public enum NestedCircleType {
        case parent
        case child
    }
}

public protocol NestedCircleLayoutDelegate: class {
    func nestedCicleLayout(sizeForItemAt item: Int) -> CGSize

    func nestedCicleLayout(itemTypeAt item: Int) -> NestedCircleLayout.NestedCircleType
}
