//
//  NestedCircleCollectionView.swift
//  iOSTemplate
//
//  Created by Professor on 2018/11/15.
//  Copyright © 2018 Agrowood. All rights reserved.
//

import UIKit

public class NestedCircleCollectionView: UICollectionView {
    @IBInspectable var parentColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }
    @IBInspectable var childColor: UIColor = .black {
        didSet {
            setNeedsDisplay()
        }
    }

    override public func awakeFromNib() {
        collectionViewLayout = NestedCircleLayout()
    }

    override public func draw(_ rect: CGRect) {
        if let layout = collectionViewLayout as? NestedCircleLayout {
            parentColor.setStroke()
            UIBezierPath(arcCenter: layout.parentCenter,
                    radius: layout.parentRadius,
                    startAngle: 0,
                    endAngle: .pi * 2,
                    clockwise: true).stroke()
            let path = UIBezierPath()
            for point in layout.childCenters() {
                path.append(UIBezierPath(arcCenter: point,
                        radius: layout.childRadius,
                        startAngle: 0,
                        endAngle: .pi * 2,
                        clockwise: true))
            }
            childColor.setStroke()
            path.stroke()
        }
    }

    override public func reloadData() {
        setNeedsDisplay()
        super.reloadData()
    }
}
